#!/usr/bin/env python
import serial
from time import sleep, time

def measure(portName = '/dev/serial0'):
    ser = serial.Serial(
        port=portName,
        baudrate = 9600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=0.2
    )

    buffer = ""
    timeStart = time()
    maxwait = .5
    msgStartChar = b"R"
    msgEndChar = b"\r"
    mm = 0;

    while time() < timeStart + maxwait:

        oneByte = ser.read(1)

        # wait for initial data prefix b'R'
        if oneByte.startswith(msgStartChar):
            # data received did not start with R reset buffer
            continue

        elif oneByte == msgEndChar:
            try:
                mm = int(buffer.lstrip('R'))
                # print (mm)
                return(mm)

            except ValueError:
                # value is not a number, reset and retry
                buffer = ""
                continue

        else:
            buffer += oneByte.decode()

    # Throw error if timeout is reached
    ser.close()
    raise RuntimeError("Expected serial data not received from ", portName)

if __name__ == '__main__':
    measurement = measure()
    print("distance =", measurement)
