# script to read left and right range sensors then produce a sound if objects detected
from time import sleep, time
import maxSensor
import os

###############################################################
# Average reaction time = rt = 0.15ms
# Time to distance = Time * velocity
# Distance to stop = D
# t = time
# v = initial velocity
# a = acceleration (or deaceleration)
# distance traveled in reaction time = (v * rt)
# D(t) = v*t - (1/2)a*t^2 + v*rt where D'(t) = 0
# D'(t) = v - a*t
# Distance traveled = v*(v/a) - (1/2)a*(v/a)^2 + v*rt = (v^2)/a + v*rt
# if v = 7mph or 3.129m/s
#     and a = 6.1m/s^2
# Distance traveled = 1.27 meters
################################################################

leftSensor = "/dev/senseLeft"
rightSensor = "/dev/senseRight"

maxRange = 5000  # change for 5m vs 10m sensor
sleepTime = .1
minMM = 9999
maxMM = 0

safe_detect = 1500

min_ignore = 350

while True: 
    try:
        mm_left = maxSensor.measure(leftSensor)
        mm_right = maxSensor.measure(rightSensor)
        print("left:", mm_left, "right:", mm_right)

        if((mm_left > min_ignore) or (mm_right > min_ignore)):
          if((mm_left < safe_detect) and ( mm_right < safe_detect)):
              print("detected  front")
              os.system('mplayer s1.m4a', shell=True)
          elif(mm_right < safe_detect):
              print("detected right")
              os.system('mplayer s2.m4a', shell=True)
          elif(mm_left < safe_detect):
              print("detected left")
              os.system('mplayer s3.m4a', shell=True)

    except Exception as e:
         print(f"ERROR: {e}")

    sleep(sleepTime)
