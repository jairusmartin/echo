# Echo

Python code that will provide audio feedback based on distance sensors mounting to an electric kart


Create env:

	python3 -m venv env

Activate
	
	source env/bin/activate

Verify Env is activated:

	which python
	
	.../env/bin/python
	

Install Requirements

	python3 -m pip install -r requirements.txt

create requirements
	
	python3 -m pip freeze

stop virtualenv

	deactivate
